package com.drxmob.driver.common.swipe_button;

public interface OnStateChangeListener {
    void onStateChange(boolean active);
}
