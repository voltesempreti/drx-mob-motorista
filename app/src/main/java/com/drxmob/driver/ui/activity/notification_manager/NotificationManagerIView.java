package com.drxmob.driver.ui.activity.notification_manager;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> managers);

    void onError(Throwable e);

}