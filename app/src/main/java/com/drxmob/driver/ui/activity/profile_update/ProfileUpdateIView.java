package com.drxmob.driver.ui.activity.profile_update;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.UserResponse;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(UserResponse user);

    void onSuccessUpdate(UserResponse object);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}
