package com.drxmob.driver.ui.activity.setting;

import com.drxmob.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
