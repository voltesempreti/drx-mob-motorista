package com.drxmob.driver.ui.activity.notification_manager;

import com.drxmob.driver.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
