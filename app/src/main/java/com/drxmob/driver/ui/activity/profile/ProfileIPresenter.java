package com.drxmob.driver.ui.activity.profile;

import com.drxmob.driver.base.MvpPresenter;

public interface ProfileIPresenter<V extends ProfileIView> extends MvpPresenter<V> {

    void getProfile();

}
