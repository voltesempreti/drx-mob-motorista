package com.drxmob.driver.ui.activity.earnings;


import com.drxmob.driver.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
