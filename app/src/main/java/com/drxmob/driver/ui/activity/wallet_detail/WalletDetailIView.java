package com.drxmob.driver.ui.activity.wallet_detail;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIView extends MvpView {
    void setAdapter(ArrayList<Transaction> myList);
}
