package com.drxmob.driver.ui.activity.help;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);

    void onError(Throwable e);
}
