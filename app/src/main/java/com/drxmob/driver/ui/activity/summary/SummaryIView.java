package com.drxmob.driver.ui.activity.summary;


import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);

    void onError(Throwable e);
}
