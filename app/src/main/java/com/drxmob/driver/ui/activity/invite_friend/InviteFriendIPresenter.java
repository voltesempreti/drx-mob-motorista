package com.drxmob.driver.ui.activity.invite_friend;

import com.drxmob.driver.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
