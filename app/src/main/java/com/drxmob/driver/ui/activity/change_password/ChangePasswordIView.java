package com.drxmob.driver.ui.activity.change_password;

import com.drxmob.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
