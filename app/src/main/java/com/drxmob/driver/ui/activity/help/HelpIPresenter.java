package com.drxmob.driver.ui.activity.help;


import com.drxmob.driver.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
