package com.drxmob.driver.ui.fragment.status_flow;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.TimerResponse;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);

    void onWaitingTimeSuccess(TimerResponse object);

    void onError(Throwable e);
}
