package com.drxmob.driver.ui.fragment.offline;

import com.drxmob.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
