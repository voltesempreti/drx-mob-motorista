package com.drxmob.driver.ui.activity.wallet_detail;

import com.drxmob.driver.base.MvpPresenter;
import com.drxmob.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}
