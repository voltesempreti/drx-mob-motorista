package com.drxmob.driver.ui.fragment.upcoming;


import com.drxmob.driver.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
