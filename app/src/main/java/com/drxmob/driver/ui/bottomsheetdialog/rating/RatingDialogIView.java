package com.drxmob.driver.ui.bottomsheetdialog.rating;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
