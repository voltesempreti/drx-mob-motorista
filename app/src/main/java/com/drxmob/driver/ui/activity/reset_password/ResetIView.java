package com.drxmob.driver.ui.activity.reset_password;

import com.drxmob.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
