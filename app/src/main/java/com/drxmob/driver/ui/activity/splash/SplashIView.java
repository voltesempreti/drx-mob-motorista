package com.drxmob.driver.ui.activity.splash;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.CheckVersion;

public interface SplashIView extends MvpView {

    void verifyAppInstalled();

    void onSuccess(Object user);

    void onSuccess(CheckVersion user);

    void onError(Throwable e);

    void onCheckVersionError(Throwable e);
}
