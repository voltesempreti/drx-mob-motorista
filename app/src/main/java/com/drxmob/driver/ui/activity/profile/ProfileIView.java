package com.drxmob.driver.ui.activity.profile;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);

    void onError(Throwable e);

}
