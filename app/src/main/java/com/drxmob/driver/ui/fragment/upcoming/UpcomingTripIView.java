package com.drxmob.driver.ui.fragment.upcoming;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
