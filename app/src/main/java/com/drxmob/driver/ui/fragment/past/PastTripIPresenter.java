package com.drxmob.driver.ui.fragment.past;


import com.drxmob.driver.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
