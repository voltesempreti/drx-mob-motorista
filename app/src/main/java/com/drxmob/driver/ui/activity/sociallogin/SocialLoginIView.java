package com.drxmob.driver.ui.activity.sociallogin;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
