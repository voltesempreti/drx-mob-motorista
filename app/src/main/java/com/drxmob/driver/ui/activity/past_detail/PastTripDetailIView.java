package com.drxmob.driver.ui.activity.past_detail;


import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
