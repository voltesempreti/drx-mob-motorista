package com.drxmob.driver.ui.activity.summary;


import com.drxmob.driver.base.MvpPresenter;

public interface SummaryIPresenter<V extends SummaryIView> extends MvpPresenter<V> {

    void getSummary(String data);
}
