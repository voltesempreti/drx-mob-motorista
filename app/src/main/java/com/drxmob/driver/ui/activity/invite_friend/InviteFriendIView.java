package com.drxmob.driver.ui.activity.invite_friend;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}
