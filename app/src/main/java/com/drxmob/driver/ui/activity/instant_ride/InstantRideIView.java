package com.drxmob.driver.ui.activity.instant_ride;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.EstimateFare;
import com.drxmob.driver.data.network.model.TripResponse;

public interface InstantRideIView extends MvpView {

    void onSuccess(EstimateFare estimateFare);

    void onSuccess(TripResponse response);

    void onError(Throwable e);

}
