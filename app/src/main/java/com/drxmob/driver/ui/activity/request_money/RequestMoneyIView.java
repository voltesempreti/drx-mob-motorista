package com.drxmob.driver.ui.activity.request_money;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
