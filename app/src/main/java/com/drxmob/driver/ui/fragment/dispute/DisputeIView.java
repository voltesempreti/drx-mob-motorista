package com.drxmob.driver.ui.fragment.dispute;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.DisputeResponse;

import java.util.List;

public interface DisputeIView extends MvpView {

    void onSuccessDispute(List<DisputeResponse> responseList);

    void onSuccess(Object object);

    void onError(Throwable e);
}
