package com.drxmob.driver.ui.activity.password;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.ForgotResponse;
import com.drxmob.driver.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);

    void onSuccess(User object);

    void onError(Throwable e);
}
