package com.drxmob.driver.ui.activity.add_card;

import com.drxmob.driver.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);

    void onError(Throwable e);
}
