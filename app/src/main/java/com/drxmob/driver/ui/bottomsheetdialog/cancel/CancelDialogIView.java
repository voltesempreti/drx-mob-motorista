package com.drxmob.driver.ui.bottomsheetdialog.cancel;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.CancelResponse;

import java.util.List;

public interface CancelDialogIView extends MvpView {

    void onSuccessCancel(Object object);
    void onError(Throwable e);
    void onSuccess(List<CancelResponse> response);
    void onReasonError(Throwable e);
}
