package com.drxmob.driver.ui.activity.wallet;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.WalletMoneyAddedResponse;
import com.drxmob.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}
