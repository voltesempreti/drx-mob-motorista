package com.drxmob.driver.ui.activity.add_card;

import com.drxmob.driver.base.MvpPresenter;

public interface AddCardIPresenter<V extends AddCardIView> extends MvpPresenter<V> {

    void addCard(String stripeToken);
}
