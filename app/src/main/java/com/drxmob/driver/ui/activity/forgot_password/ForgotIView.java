package com.drxmob.driver.ui.activity.forgot_password;

import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
