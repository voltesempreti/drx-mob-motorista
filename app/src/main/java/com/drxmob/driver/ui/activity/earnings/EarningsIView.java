package com.drxmob.driver.ui.activity.earnings;


import com.drxmob.driver.base.MvpView;
import com.drxmob.driver.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);

    void onError(Throwable e);
}
