/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.drxmob.driver;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.drxmob.driver";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 6;
  public static final String VERSION_NAME = "1.0.5";
  // Fields from default config.
  public static final String BASE_IMAGE_URL = "https://drxmob.com.br/storage/";
  public static final String BASE_PAY_URL = "https://drxmob.com.br/index.php";
  public static final String BASE_URL = "https://drxmob.com.br/";
  public static final String CLIENT_ID = "2";
  public static final String CLIENT_SECRET = "taHdKq3goXAkwavriUFZQVXbHag1AxyJseNCVgiE";
  public static final String DEVICE_TYPE = "android";
  public static final String FCM_SERRVER_KEY = "AAAAIbY0xsk:APA91bGFzj_WrSrWXTSSuVhZMGj-Pq3tBuGO3tiHRJDyni9ck40x2i4tKRp0B5flBEpGVXmbiIhIAbOUpoVFAkYaCfTX4RdyYCEaQ0r23Q0JvhCEtk5fsCWMG_zKsrBrVbfXxkPbzvbn";
  public static final String HELP_URL = "https://drxmob.com.br/help";
  public static final String TERMS_CONDITIONS = "https://drxmob.com.br/privacy";
  public static final String USER_PACKAGE = "com.drxmob.user";
}
